#ifndef BASE64_H
#define BASE64_H

#include <stddef.h>

char *base64_encode(unsigned char *src, size_t len, char *res, size_t max_len);
size_t base64_decode(char *src, size_t len, unsigned char *res, size_t max_len);

#endif // BASE64_H
