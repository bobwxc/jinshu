#ifndef SOCKET_H
#define SOCKET_H

#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#define DEFAULE_BACKLOG 512

int init_ipv4_socket(char *ip_address, short port);
int init_ipv6_socket(char *ip_address, short port);
int close_socket(int sfd);

#endif // SOCKET_H