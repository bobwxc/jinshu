#ifndef CHECK_H
#define CHECK_H

int check_local_part_format(unsigned char *str, int str_len);
int check_domain_format(unsigned char *str, int str_len);
int check_address_literal_format(unsigned char *str, int str_len);
int check_reverse_path_format(unsigned char *str, int str_len);

int check_reverse_path(unsigned char *str, int str_len);
int check_forward_path(unsigned char *str, int str_len);

size_t get_declare_size(unsigned char *str, int str_len);

#endif // CHECK_H
