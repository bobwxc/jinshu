#ifndef JTHREADS_H
#define JTHREADS_H

#include <pthread.h>

#include "cas_queue.h"

typedef struct handle_thread
{
    pthread_t thread;
    int epfd;                    // 等待的epoll文件符
    q_queue_t output_mail_queue; // 输出队列
} handle_thread_t;

typedef struct accept_thread
{
    pthread_t thread;
    int socket_epfd; // 监听
    int handle_epfd; // 处理
} accept_thread_t;

enum output_type
{
    OPT_MBOX,
    OPT_SOCKET,
    OPT_SMTP
};

typedef struct output_thread
{
    pthread_t thread;
    pthread_cond_t waiting_new_mail;
    pthread_mutex_t working_new_mail;

    q_queue_t output_mail_queue; // 输出邮件队列

    enum output_type output_type;
    int output_fd;
} output_thread_t;

typedef struct watchdog_thread
{
    pthread_t thread;
} watchdog_thread_t;

int init_handle_threads(short thread_num, handle_thread_t *handle_threads, void *(*start_fn)(void *), int epfd);
int init_accept_threads(short thread_num, accept_thread_t *accept_threads, void *(*start_fn)(void *), int socket_epfd, int handle_epfd);
int init_output_threads(short thread_num, output_thread_t *output_threads, void *(*start_fn)(void *), enum output_type output_type, int output_fd);

#endif // JTHREADS_H
