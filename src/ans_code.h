#ifndef ANS_CODE_H
#define ANS_CODE_H

// SMTP server answer code and string

typedef enum ans_code
{
    a211,
    a214,
    a220,
    a221,
    a250,
    a250EHLO,
    a251,
    a252,
    a354,
    a421,
    a450,
    a451,
    a452,
    a500,
    a501,
    a502,
    a503,
    a504,
    a550,
    a551,
    a552,
    a553,
    a554
} ans_code_t;

extern unsigned char ans_string[23][128];

int init_ans_string();

#endif // ANS_CODE_H
