#ifndef CONFIG_DB_H
#define CONFIG_DB_H

#include "../contrib/sqlite3/sqlite3.h"

int init_config_db(unsigned char *config_db_path, sqlite3 **config_db_pp);
int close_config_db(sqlite3 *config_db);

int get_config_var(sqlite3 *config_db, unsigned char *key, unsigned char *res, int res_max_len);
int set_config_var(sqlite3 *config_db, unsigned char *key, unsigned char *var);
int del_config_var(sqlite3 *config_db, unsigned char *key);

typedef enum domain_ctrl_type
{
    DOMAIN_UNKNOW = -1, // can't be set, only for select.
    DOMAIN_DENY,
    DOMAIN_ALLOW
} domain_ctrl_type_t;

domain_ctrl_type_t get_domain_ctrl(sqlite3 *config_db, unsigned char *user, unsigned char *domain);
int set_domain_ctrl(sqlite3 *config_db, domain_ctrl_type_t type, unsigned char *user, unsigned char *domain);
int del_domain_ctrl(sqlite3 *config_db, unsigned char *user, unsigned char *domain);

#endif // CONFIG_DB_H
