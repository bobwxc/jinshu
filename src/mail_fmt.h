#ifndef MAIL_FMT_H
#define MAIL_FMT_H

#include <time.h>

typedef struct mail_store
{
    time_t recv_time;
    unsigned char ip[48]; // ipv6 addr prefix "IPv6:"
    unsigned char id[48];
    unsigned char hostname[256]; // HELO/EHLO
    unsigned char mailfrom[512]; // Reverse-path
    short rcpt_n;
    unsigned char rcptto[100][512]; // Forward-path
    size_t data_len;
    unsigned char *data;
} mail_store_t;

mail_store_t *init_mail_store(mail_store_t *ms);
void destory_mail_store(mail_store_t *mail_store);

unsigned char *gen_mail_id(mail_store_t *mail_store, unsigned char *r, int max_len);

unsigned char *gen_return_path_line(unsigned char *mailfrom);
unsigned char *gen_received_line(mail_store_t *mail_store, unsigned char *rs, size_t max_len);

#endif // MAIL_FMT_H
