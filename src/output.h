#ifndef OUTPUT_H
#define OUTPUT_H

#include "jthreads.h"

int prepare_output_fd(enum output_type output_type, const char *output_path);
int *output_start(output_thread_t *thread);

#endif // OUTPUT_H

