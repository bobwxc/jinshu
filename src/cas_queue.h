#ifndef Q_QUEUE_H
#define Q_QUEUE_H

typedef struct q_node q_node_t;
typedef struct q_queue q_queue_t;

typedef struct q_node
{
    void *data;
    q_node_t *next;
    q_queue_t *belong_queue;
} q_node_t;

typedef struct q_queue
{
    q_node_t *head;
    q_node_t *tail;
} q_queue_t;

q_queue_t *q_init(q_queue_t *q);
int q_push(q_queue_t *q, void *data);
int q_pop(q_queue_t *q, void **data);

#endif // Q_QUEUE_H
