#ifndef HANDLE_H
#define HANDLE_H

#include <time.h>
#include <stdatomic.h>
#include <sys/socket.h>
#include <sys/epoll.h>

#include "ans_code.h"
#include "jthreads.h"
#include "mail_fmt.h"

typedef struct income
{
	int fd;
	struct sockaddr_storage addr;
	int belong_epfd;
	time_t accept_time;
	ans_code_t ans_code;	
	short recv_DATA_flag; // =0 before, =1 receiving DATA, =2 received
	size_t declare_size; // delcare size in MAIL FROM:<> SIZE=???
	mail_store_t *mail_store;
	// short unhandle_n;
	// unsigned char unhandle_buf[512];
} income_t;

int *handle_start(handle_thread_t *thread);

#endif // HANDLE_H