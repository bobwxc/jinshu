#ifndef GLOBAL_VAR_H
#define GLOBAL_VAR_H

#include "config_db.h"

extern unsigned char g_log_file_path[128];
extern char g_hostname[128];
extern char g_hostdomain[128];
extern char g_ipv4_addr[16];
extern char g_ipv6_addr[48];
extern short g_port;

extern sqlite3 *g_config_db;

extern short g_handle_threads_num;
extern short g_accept_threads_num;
extern short g_output_threads_num;

extern size_t g_max_mail_size;

void init_global_var(int argc, char **argv);

#endif // GLOBAL_VAR_H