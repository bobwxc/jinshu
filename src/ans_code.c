#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "ans_code.h"
#include "global_var.h"

unsigned char ans_string[23][128] = {
    "211 System status, or system help reply\r\n",
    "214 Help message\r\n",
    "220 Jinshu mail service ready\r\n",
    "221 Jinshu mail service closing transmission channel\r\n",
    "250 OK\r\n",
    "250 OK\r\n250 SIZE 33554432\r\n250 8BITMIME\r\n",
    "251 User not local; will forward to <forward-path>\r\n",
    "252 Cannot VRFY user\r\n",
    "354 Start mail input; end with <CRLF>.<CRLF>\r\n",
    "421 Jinshu mail service not available, closing transmission channel\r\n",
    "450 Mailbox unavailable\r\n",
    "451 Requested action aborted: local error in processing\r\n",
    "452 Insufficient system storage\r\n",
    "500 Syntax error, command unrecognized\r\n",
    "501 Syntax error in parameters or arguments\r\n",
    "502 Command not implemented\r\n",
    "503 Bad sequence of commands\r\n",
    "504 Command parameter not implemented\r\n",
    "550 Mailbox unavailable\r\n",
    "551 User not local; please try <forward-path>\r\n",
    "552 Exceeded storage allocation\r\n",
    "553 Mailbox name not allowed\r\n",
    "554 Transaction failed\r\n",
};

// must be called after init global var
int init_ans_string()
{
    int rc = 0;
    memset(ans_string[a250EHLO], 0, 128);
    rc = snprintf(ans_string[a250EHLO], 128, "250 OK\r\n250 SIZE %zu\r\n250 8BITMIME\r\n", g_max_mail_size);
    if (rc < 0)
        return -1;

    return 0;
}