
# ![锦书 logo](./jinshu.svg)

> 云中谁寄锦书来，雁字回时，月满西楼。

## 用途

邮件接收服务器

## 架构

## 代码目录结构

## 功能实现

### ESMTP

- [x] SIZE 1870 已实现
- [x] 8BITMIME 6152 已实现
- [ ] DSN 3461 太复杂
- [x] MT-PRIORITY 6710 拒绝
- [x] DELIVERBY 2852 拒绝
- [ ] PIPELINING 2920 等待改造buf
- [ ] CHUNKING 3030
- [ ] BINARYMIME 3030
- [ ] SMTPUTF8 6531

## TODO

- [ ] send/recv ERRNO 处理
- [x] check DATA size
- [ ] signal handle
- [ ] output thread
- [ ] watchdog thread
