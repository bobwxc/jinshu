
make	= gmake

all: bin

bin:
	$(make) -C src/
	mv src/jinshu ./

clean:
	$(make) -C src/ clean
	-rm ./jinshu
